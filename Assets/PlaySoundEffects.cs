﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundEffects : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySoundEffect(string name)
    {
        SonicBloom.Koreo.AudioManager audioManager = SonicBloom.Koreo.AudioManager.GetAudioManager();
        if (audioManager != null)
        {
            audioManager.PlayAudioClip(name);
        }
    }
}
