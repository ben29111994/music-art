﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SonicBloom.Koreo
{
    public class GameOverScreenController : MonoBehaviour
    {
        [SerializeField]
        GameObject m_startButton;

        // Use this for initialization
        void Start()
        {
            m_startButton.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartButtonPressed()
        {
            Debug.Log("pressed ok button");

		    AudioManager.GetAudioManager().PlayAudioClip ("start");

            // player has played the game before. Go into level select.
            SceneManager.LoadScene("jigsaw Level Select");
        }

    }
}
