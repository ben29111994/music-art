﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AdjustDragThreshold : MonoBehaviour {

    void Start()
    {
        EventSystem es = GetComponent<EventSystem>();
        if (es != null)
        {
            int defaultValue = es.pixelDragThreshold;
            es.pixelDragThreshold = 20; // 500;
            //es.pixelDragThreshold = Mathf.Max(
            //     defaultValue,
            //     (int)(defaultValue * Screen.dpi / 160f));
        }
    }

}
