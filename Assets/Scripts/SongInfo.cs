﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace SonicBloom.Koreo
{
    public class SongInfo : MonoBehaviour
    {
        public static SongInfo songInfoInstance;
        MenuManager uiManager;
        public Text IDText;
        public Text NameText;
        public Text numberProgress;
        public Image star1;
        public Image star2;
        public Image star3;
        public float score;
        public float progress;
        public  float eventCount;
        public Koreography koreography;
        private KoreographyTrackBase koreographyTrackBase;
        List<KoreographyEvent> listEvent = new List<KoreographyEvent>();
        public GameObject buttonNormal;
        public GameObject buttonBuySong;
        public Image buttonTrialPlay;
        AudioSource trialMusic;
        Tween myTween;
        public Image background;
        Color color;
        int price;
        public GameObject BlockPanel;
        public GameObject getData;
        public GameObject buttonFreeTrial;

        // Use this for initialization
        void Start()
        {
            songInfoInstance = this;
            trialMusic = GetComponent<AudioSource>();
            int id = int.Parse(IDText.text.ToString());
            var unlockStatus = PlayerPrefs.GetInt(IDText.text.ToString() + "unlock");
            if (unlockStatus == 1 || id == 1)
            {
                buttonNormal.SetActive(true);
                buttonBuySong.SetActive(false);
                buttonFreeTrial.gameObject.SetActive(false);
            }
            else
            {
                buttonBuySong.SetActive(true);
                star1.transform.parent.gameObject.SetActive(false);
                star2.transform.parent.gameObject.SetActive(false);
                star3.transform.parent.gameObject.SetActive(false);
                price = id * 100;
                buttonBuySong.GetComponentInChildren<Text>().text = price.ToString();
                buttonNormal.SetActive(false);

                BlockPanel.SetActive(true);
            }

            //For auto generate songs list
            uiManager = FindObjectOfType<MenuManager>();
            koreography = uiManager.koreographies[int.Parse(IDText.text.ToString()) - 1];
            koreographyTrackBase = koreography.GetTrackByID(NameText.text.ToString());
            listEvent = koreographyTrackBase.GetAllEvents();
            eventCount = listEvent.Count;
            score = PlayerPrefs.GetFloat(IDText.text.ToString()+"score");
            progress = /*PlayerPrefs.GetFloat(IDText.text.ToString() + "progress");*/ ((int)(score * 100 / eventCount));
            numberProgress.text = progress.ToString() + "%";
            var scoreProgress = score / eventCount;
            if(scoreProgress > 0.66f)
            {
                star1.fillAmount = 1;
                star2.fillAmount = 1;
                star3.fillAmount = (scoreProgress - 0.66f) * 100 / 33.3f;
            }
            else if (scoreProgress > 0.33f)
            {
                star1.fillAmount = 1;
                star2.fillAmount = (scoreProgress - 0.33f) * 100 / 33.3f;
            }
            else if (scoreProgress < 0.33f)
            {
                star1.fillAmount = scoreProgress * 100 / 33.3f;
            }

            StartCoroutine(delayLockArt());
        }

        public void OnButtonClick()
        {
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            //int currentlife = PlayerPrefs.GetInt("life");
            uiManager.OnChooseSongButton(this.gameObject);

            MenuManager.MenuManagerInstance.OnButtonPress();
        }

        public void OnButtonBuySong()
        {
            var currentGem = PlayerPrefs.GetInt("gem");
            if(currentGem >= price)
            {
                MenuManager.MenuManagerInstance.OnButtonPress();
                currentGem -= price;
                PlayerPrefs.SetInt("gem", currentGem);
                PlayerPrefs.SetInt(IDText.text.ToString() + "unlock", 1);
                UIManager.instance.gemText.text = currentGem.ToString();
                buttonNormal.SetActive(true);
                buttonBuySong.SetActive(false);
                star1.transform.parent.gameObject.SetActive(true);
                star2.transform.parent.gameObject.SetActive(true);
                star3.transform.parent.gameObject.SetActive(true);
            }
            else
                SoundManager.instance.PlaySound(SoundManager.instance.invalid);
        }

        public void OnButtonTrySong()
        {
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            //int currentlife = PlayerPrefs.GetInt("life");
            uiManager.OnChooseSongButton(this.gameObject);
            SpawnItem.isTrying = true;

            MenuManager.MenuManagerInstance.OnButtonPress();
        }

        public void OnButtonTrialPlay()
        {
            if (!trialMusic.isPlaying)
            {
                trialMusic.clip = MenuManager.MenuManagerInstance.songs[int.Parse(IDText.text.ToString())];
                AudioSource[] allAudioSources = FindObjectsOfType<AudioSource>();
                foreach (var item in allAudioSources)
                {
                    item.Stop();
                }
                trialMusic.Play();
                var color1 = new Color32(246, 193, 34, 255);
                buttonTrialPlay.color = color1;
                var color2 = new Color32(246, 193, 34, 50);
                myTween = buttonTrialPlay.DOColor(color2, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                trialMusic.Stop();
                UIManager.instance.bgm.Play();
                var color = new Color32(246, 193, 34, 255);
                myTween.Kill();
                buttonTrialPlay.color = color;
            }
        }

        IEnumerator delayLockArt()
        {
            yield return new WaitForSeconds(2);
            getData = this.gameObject.transform.Find("Level Group(Clone)/level button(Clone)/background").gameObject;
            if (progress < 100)
            {
                getData.GetComponent<Button>().interactable = false;
            }
        }

        private void FixedUpdate()
        {
            if(trialMusic.time > 10)
            {
                trialMusic.Stop();
                UIManager.instance.bgm.Play();
                var color = new Color32(246, 193, 34, 255);
                myTween.Kill();
                buttonTrialPlay.color = color;
            }
            if(!trialMusic.isPlaying)
            {
                var color = new Color32(246, 193, 34, 255);
                myTween.Kill();
                buttonTrialPlay.color = color;
            }
        }
    }
}
