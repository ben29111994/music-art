﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DG.Tweening;

namespace SonicBloom.Koreo
{
    public class UIManager : MonoBehaviour {

        public static UIManager instance;

        [Header(" menu ")]
        public GameObject taskBar;
        public GameObject menuSong;
        public GameObject menuPlane;
        public GameObject menuStore;
        public GameObject menuInfo;

        public Text gemText;
        public Text gemTextInGame;
        public GameObject currentChoose;

        bool isDelay;

        [Header(" button ")]
        public GameObject buttonSong;
        public GameObject buttonPlane;
        public GameObject buttonStore;
        public GameObject buttonInfo;

        public AudioSource bgm;
        public Button songBtn;
        public Color colorChoose;
        public GameObject loadingPanel;
        public GameObject artCollection;
        public GameObject artCollectionContent;
        public GameObject viewArt;

        void Awake() {
            instance = this;
            if (PlayerPrefs.GetInt("isFirstTime") == 0)
            {
                PlayerPrefs.SetInt("life", 10);
                PlayerPrefs.SetInt("isFirstTime", 1);
            }
        }

        void Start()
        {
            bgm.Play();
            gemText.text = PlayerPrefs.GetInt("gem").ToString();
            gemTextInGame.text = gemText.text;
        }

        public void BtnPlane() {
            if (!bgm.isPlaying)
            {
                bgm.Play();
            }
            ColorBlock cb = songBtn.colors;
            cb.normalColor = colorChoose;
            songBtn.colors = cb;
            menuPlane.SetActive(true);
            menuSong.SetActive(false);
            menuStore.SetActive(false);
            menuInfo.SetActive(false);

            iTween.ScaleTo(buttonSong, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonStore, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonInfo, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            iTween.ScaleTo(button, new Vector3(1, 1, 1), 0.3f);
            currentChoose.transform.DOLocalMoveX(-125, 0.2f);
        }

        public void BtnSongs() {
            menuSong.SetActive(true);
            menuPlane.SetActive(false);
            menuStore.SetActive(false);
            menuInfo.SetActive(false);

            iTween.ScaleTo(buttonPlane, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonStore, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonInfo, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            iTween.ScaleTo(button, new Vector3(1, 1, 1), 0.3f);
            currentChoose.transform.DOLocalMoveX(-400, 0.2f);
        }

        public void BtnStores() {
            if (!bgm.isPlaying)
            {
                bgm.Play();
            }
            ColorBlock cb = songBtn.colors;
            cb.normalColor = colorChoose;
            songBtn.colors = cb;
            menuStore.SetActive(true);
            menuPlane.SetActive(false);
            menuSong.SetActive(false);
            menuInfo.SetActive(false);

            iTween.ScaleTo(buttonSong, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonPlane, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonInfo, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            iTween.ScaleTo(button, new Vector3(1, 1, 1), 0.3f);
            currentChoose.transform.DOLocalMoveX(140, 0.2f);
        }


        public void BtnInfomation() {
            if (!bgm.isPlaying)
            {
                bgm.Play();
            }
            ColorBlock cb = songBtn.colors;
            cb.normalColor = colorChoose;
            songBtn.colors = cb;
            menuInfo.SetActive(true);
            menuPlane.SetActive(false);
            menuSong.SetActive(false);
            menuStore.SetActive(false);

            iTween.ScaleTo(buttonSong, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonPlane, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            iTween.ScaleTo(buttonStore, new Vector3(0.8f, 0.8f, 0.8f), 0.3f);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            iTween.ScaleTo(button, new Vector3(1, 1, 1), 0.3f);
            currentChoose.transform.DOLocalMoveX(400, 0.2f);
        }

        public void BtnHelixHoop() {
#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1327168260");
#endif
        }

        public void BtnKingBallIO() {
#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1395136315");
#endif
        }

        public void BtnGetLife_ViewAds() {
            MenuManager.MenuManagerInstance.isGemOrLifeOrRevive = 1;
            UnityAdsManager.Instance.ShowRewardAds();
        }

        public void BtnGetGem_ViewAds()
        {
            MenuManager.MenuManagerInstance.isGemOrLifeOrRevive = 0;
            UnityAdsManager.Instance.ShowRewardAds();
        }

        public void CompleteViewAds() {
            if (MenuManager.MenuManagerInstance.isGemOrLifeOrRevive == 0)
            {
                BtnGetGem();
            }
            else if (MenuManager.MenuManagerInstance.isGemOrLifeOrRevive == 2)
            {
                PlayerControl.PlayerControlInstance.OnResume();
            }
            SoundManager.instance.PlaySound(SoundManager.instance.collect);
        }

        public void BtnFullUnlock_IAP()
        {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);

            PlayerPrefs.SetInt("RemoveAds", 1);
            PlayerPrefs.SetInt("FullUnlock", 1);
            for (int i = 1; i <= 15; i++)
            {
                PlayerPrefs.SetInt(i + "unlock", 1);
                if (i < 3)
                {
                    PlayerPrefs.SetInt("ship" + i, 1);
                }
            }
            Color color = new Color32(255, 255, 255, 255);
            MenuManager.MenuManagerInstance.flashPanel.GetComponent<Image>().DOColor(color, 1);
            SceneManager.LoadScene("Main");
        }

        public void BtnRemoveAds_IAP() {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);

            PlayerPrefs.SetInt("RemoveAds", 1);
        }

        public void BtnGetGem()
        {
            var gem = PlayerPrefs.GetInt("gem");
            gem += 20;
            PlayerPrefs.SetInt("gem", gem);
            if(UIManager.instance.gemText!=null)
                UIManager.instance.gemText.text = gem.ToString();
        }

        public void BtnBuy120Gems_IAP() {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);
            PlusGem(120);
        }

        public void BtnBuy250Gems_IAP() {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);
            PlusGem(250);
        }

        public void BtnBuy400Gems_IAP() {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);
            PlusGem(400);
        }

        public void BtnBuy800Gems_IAP() {
            SoundManager.instance.PlaySound(SoundManager.instance.collect);
            PlusGem(800);
        }

        public void BtnSoundIAP() {

            SoundManager.instance.PlaySound(SoundManager.instance.star);

        }

        public void PlusGem(int num)
        {
            var gem = PlayerPrefs.GetInt("gem");
            gem += num;
            PlayerPrefs.SetInt("gem", gem);
            UIManager.instance.gemText.text = gem.ToString();
        }

        public void ButtonAbout()
        {
            Application.OpenURL(AppInfo.Instance.FACEBOOK_LINK);
        }

        public void ButtonRate()
        {
#if UNITY_ANDROID
            Application.OpenURL(AppInfo.Instance.PLAYSTORE_LINK);
#elif UNITY_IOS
        Application.OpenURL(AppInfo.Instance.APPSTORE_LINK);
#endif
            PlayerPrefs.SetInt("isRate", 1);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            if (button.name == "BtnOK")
                button.GetComponentInParent<Transform>().gameObject.SetActive(false);
        }

        //public void OnArtCollectionClick()
        //{
        //    viewArt.SetActive(true);
        //    var button = EventSystem.current.currentSelectedGameObject.gameObject;
        //    viewArt.gameObject.transform.GetChild(0).GetComponent<Text>().text = button.gameObject.transform.GetChild(0).GetComponent<Text>().text;
        //    viewArt.gameObject.transform.GetChild(1).GetComponent<Text>().text = button.gameObject.transform.GetChild(1).GetComponent<Text>().text;
        //    viewArt.gameObject.transform.GetChild(2).GetComponent<Text>().text = button.gameObject.transform.GetChild(2).GetComponent<Text>().text;
        //    viewArt.gameObject.transform.GetChild(0).GetComponent<RawImage>().texture = button.gameObject.transform.GetChild(0).GetComponent<RawImage>().texture;
        //}

        public void OnCloseViewArt()
        {
            artCollectionContent.SetActive(true);
            viewArt.SetActive(false);
        }
    }
}
