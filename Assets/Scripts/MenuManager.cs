﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

namespace SonicBloom.Koreo
{
    public class MenuManager : MonoBehaviour
    {
        public static MenuManager MenuManagerInstance;
        public List<Koreography> koreographies;
        public AudioClip[] songs;
        public GameObject spawnItem;
        public GameObject MusicPlayer;
        public GameObject SongPanel1;
        public GameObject GridView;
        public GameObject ScrollSnap;
        public GameObject Menu;
        public BlurOptimized screenBlur;
        public SimpleMusicPlayer simpleMusicPlayer;
        public AudioSource UIAudio;
        public GameObject flashPanel;
        public static bool isHard;
        public GameObject PauseMenu;
        public AudioSource MainMusic;
        public Text ProgressPercent;
        public GameObject ContinueMenu;
        public GameObject GameOverMenu;
        public Text songName;
        public Image star1;
        public Image star2;
        public Image star3;
        public Text nameScoreBoard;
        public Text numberScoreBoard;
        public int isGemOrLifeOrRevive = 2;
        GameObject[] artLevelList;
        public GameObject contentArt;
        public GameObject getParentObject;
        public GameObject getArtParentObject;
        public GameObject getData;
        public GameObject getArtData;
        public GameObject[] artData;
        public GameObject[] ArtCollection;
        public GameObject ArtContent;
        public GameObject questionScrollView;
        public GameObject gameplayPanel;
        public GameObject gameplayManager;
        public GameObject gameplayController;
        public GameObject unlockPanel;
        public Text gemText;
        public Text nextSongName;
        public static int currentGameLevel = 1;
        public static bool isLoadCollection = false;

        // Use this for initialization
        void Awake()
        {
            isLoadCollection = false;
            //PlayerPrefs.DeleteAll();
            simpleMusicPlayer = MusicPlayer.GetComponent<SimpleMusicPlayer>();
            MenuManagerInstance = this;
            Color color = new Color32(0, 0, 0, 0);
            flashPanel.GetComponent<Image>().DOColor(color, 1);

            //For auto generate songs list
            if(SpawnItem.isPlaying)
            {
                //Menu.SetActive(false);
                //gameplayController.SetActive(false);
                //gameplayManager.SetActive(false);
                Destroy(Menu);
                Destroy(gameplayController);
                Destroy(gameplayManager);
            }

            int i = 0;
            foreach (var item in koreographies)
            {
                i++;
                GameObject songPanel;
                songPanel = Instantiate(SongPanel1) as GameObject;
                songPanel.transform.SetParent(GridView.transform);
                songPanel.transform.localScale = new Vector3(1, 1, 1);
                songPanel.GetComponent<SongInfo>().IDText.text = i.ToString();
                songPanel.GetComponent<SongInfo>().NameText.text = songs[i].ToString().Replace(" (UnityEngine.AudioClip)", "");
            }

            ScrollSnap.SetActive(true);
            screenBlur = GameObject.FindObjectOfType<BlurOptimized>();
            var id = PlayerPrefs.GetInt("id");
            var name = PlayerPrefs.GetString("name");
            if (id == 0)
            {
                id = 1;
            }
            if (name == null)
            {
                name = "Dubstep";
            }
            //Debug.Log(name);
            var music = spawnItem.GetComponent<AudioSource>();
            music.clip = songs[id];
            spawnItem.GetComponent<SpawnItem>().eventID = name;
            simpleMusicPlayer.LoadSong(koreographies[id - 1]);
            StartCoroutine(waitForArtLoad());
            for(int j = 0; j < ArtCollection.Length; j++)
            {
                if(j > GetArtData.Instance.imageList.Count - 1)
                {
                    ArtCollection[j].SetActive(false);
                }
                else
                ArtCollection[j].GetComponent<RawImage>().texture = GetArtData.Instance.imageList[j];
            }
            var gem = PlayerPrefs.GetInt("gem");
            gemText.text = gem.ToString();
        }

        public void FixedUpdate()
        {
            //if (SpawnItem.isPlaying)
            //{
            //    Menu.SetActive(false);
            //}
        }

        public void OnPauseButton()
        {
            SpawnItem.isMusicStart = false;
            OnButtonPress();
            PauseMenu.SetActive(true);
            screenBlur.enabled = true;
            simpleMusicPlayer.Pause();
            MainMusic.Pause();
            Time.timeScale = 0f;
        }

        public void OnResumeButton()
        {
            Time.timeScale = 1;
            MainMusic.UnPause();
            simpleMusicPlayer.Play();
            OnButtonPress();
            PauseMenu.SetActive(false);
            screenBlur.enabled = false;
            if (SpawnItem.SpawnItemInstance.audioCom.isPlaying)
            {
                SpawnItem.isMusicStart = true;
            }
        }

        public void OnRestartButton()
        {
            GetArtData.Instance.artGamePlayObjects[0].transform.parent = AppInfo.Instance.transform;
            GetArtData.Instance.artGamePlayObjects[1].SetActive(true);
            GetArtData.Instance.artGamePlayObjects[1].transform.parent = AppInfo.Instance.transform;
            GetArtData.Instance.artGamePlayObjects[2].transform.parent = AppInfo.Instance.transform;
            Time.timeScale = 1;
            OnButtonPress();
            PauseMenu.SetActive(false);
            screenBlur.enabled = false;
            Color color = new Color32(255, 255, 255, 255);
            flashPanel.GetComponent<Image>().DOColor(color, 1);
            SpawnItem.isPlaying = true;
            SceneManager.LoadScene("Main");
        }

        public void BtnRevive_ViewAds()
        {
            isGemOrLifeOrRevive = 2;
            UnityAdsManager.Instance.ShowRewardAds();
        }

        public void OnHomeButton()
        {
            Time.timeScale = 1;
            OnButtonPress();
            Color color = new Color32(255, 255, 255, 255);
            flashPanel.GetComponent<Image>().DOColor(color, 1);
            SpawnItem.isPlaying = false;
            SceneManager.LoadScene("Main");
            screenBlur.enabled = true;
        }

        public void OnChooseSongButton(GameObject button)
        {
            Color color = new Color32(255, 255, 255, 255);
            if (flashPanel != null)
            {
                flashPanel.GetComponent<Image>().DOColor(color, 1);
            }
            SpawnItem.isPlaying = true;
            getParentObject = button;
            StartCoroutine(delayLoadArtData(button));
        }

        IEnumerator delayLoadArtData(GameObject button)
        {
            if (UIManager.instance.loadingPanel != null)
            {
                UIManager.instance.loadingPanel.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
                //var childObject = UIManager.instance.loadingPanel.transform.GetChild(0);
                //childObject.GetComponent<Image>().color = new Color32(0, 0, 0, 0);
                UIManager.instance.loadingPanel.SetActive(true);
            }
            getData = getParentObject.transform.Find("Level Group(Clone)/level button(Clone)").gameObject;
            getData.GetComponent<LevelButton>().PressedButton(false);
            yield return new WaitForSeconds(0.5f);
            getArtData = getArtParentObject.transform.Find("level button(Clone)").gameObject;
            getArtData.GetComponent<LevelButton>().PressedButton(false);
            yield return new WaitForSeconds(0.5f);
            gameplayPanel.transform.parent = AppInfo.Instance.transform;
            gameplayManager.SetActive(true);
            gameplayManager.transform.parent = AppInfo.Instance.transform;
            gameplayController.transform.parent = AppInfo.Instance.transform;
            yield return new WaitForSeconds(1);
            artData = GameObject.FindGameObjectsWithTag("ArtData");
            for (int i = 0; i < artData.Length; i++)
            {
                GetArtData.Instance.AddData(artData[i]);
            }
            //Debug.LogError("Run");
            SceneManager.LoadScene("Main");
            if(SpawnItem.isTrying)
            {
                StartCoroutine(delayPlayTrial());
            }
            UIManager.instance.gameObject.SetActive(false);

            var id = int.Parse(button.GetComponent<SongInfo>().IDText.text.ToString());
            string name = button.GetComponent<SongInfo>().NameText.text.ToString();
            PlayerPrefs.SetInt("id", id);
            PlayerPrefs.SetString("name", name);
        }

        IEnumerator delayPlayTrial()
        {
            yield return new WaitForSeconds(5);
            OnHomeButton();
        }

        public void OnButtonPress()
        {
            UIAudio.Play();
            //AudioListener.volume = 0f;
        }

        IEnumerator waitForArtLoad()
        {
            yield return new WaitForSeconds(0.5f);
            int j = 0;
            if (!SpawnItem.isPlaying)
            {
                while (j < koreographies.Count)
                {
                    var currentObject = contentArt.transform.GetChild(0);
                    currentObject.transform.SetParent(GridView.transform.GetChild(j));
                    currentObject.transform.localPosition = new Vector2(-130, currentObject.transform.localPosition.y);
                    j++;
                }
                contentArt.SetActive(false);
            }
            isLoadCollection = true;
        }

        public void delayShowArtData()
        {
            StartCoroutine(delayShowArtDataAfterClick());
        }

        IEnumerator delayShowArtDataAfterClick()
        {
            yield return new WaitForSeconds(0.5f);
            var num = getArtParentObject.transform.childCount;
            for (int i = 0; i < num; i++)
            {
                getArtData = getArtParentObject.transform.GetChild(i).gameObject;
                if (getArtData.name == "level button(Clone)" && getArtData.GetComponent<LevelButton>().isFinished == false)
                {
                    getArtData.GetComponent<LevelButton>().PressedButton(false);
                    break;
                }
                if (i == num - 1)
                {
                    getArtData = getArtParentObject.transform.GetChild(0).gameObject;
                    getArtData.GetComponent<LevelButton>().PressedButton(false);
                }
            }

            //getArtData = getArtParentObject.transform.Find("level button(Clone)").gameObject;
            //if (getArtData.GetComponent<LevelButton>().isFinished == false)
            //{
            //    getArtData.GetComponent<LevelButton>().PressedButton(false);
            //}
        }
    }
}
