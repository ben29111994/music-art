﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonicBloom.Koreo
{
	public class TutorialManager : MonoBehaviour {

	    [SerializeField]
	    GameObject[] m_tutorialRoots;

		static TutorialManager m_instance = null;


		public enum MessageID
		{
			DRAG_TILES_TO_SWAP_POSITION = 0,
			HIDDEN_TILES,
			BOMB_TILES,
			LAST
		}

		public static TutorialManager Instance
		{
			get
			{
				return m_instance;
			}
		}




		// Use this for initialization
		void Start ()
	    {
			m_instance = this;
	        ClearAllTutorialMessages();
	    }
		
		// Update is called once per frame
		void Update () {
			
		}

		public bool HasTutorialBeenShown (MessageID tutorialID)
		{
//			if (GameStateClass.Instance != null)
//			{
//				Debug.Log("TutorialHasBeenShown = "+GameStateClass.Instance.TutorialHasBeenShown+", m_tutorialsAlreadyShown = "+m_tutorialsAlreadyShown[(int)tutorialID]);
//				return GameStateClass.Instance.TutorialHasBeenShown || m_tutorialsAlreadyShown [(int)tutorialID];
//			}
			return PlayerProgress.GetPlayerProgress().m_tutorialsAlreadyShown [(int)tutorialID];
		}

		public void TutorialHasBeenShown (MessageID tutorialID)
		{
			PlayerProgress.GetPlayerProgress().m_tutorialsAlreadyShown [(int)tutorialID] = true;
			PlayerProgress.GetPlayerProgress().WriteStateToJSONFile();

//			bool allTutorialsShown = true;
//			foreach(bool b in m_tutorialsAlreadyShown)
//			{
//				allTutorialsShown = allTutorialsShown && b;
//			}
//
//			if (allTutorialsShown)
//			{
//				if (GameStateClass.Instance != null)
//				{
//					GameStateClass.Instance.TutorialHasBeenShown = true;
//				}
//			}
		}

		public void SetTutorialAlreadyShown(int n, bool state)
		{
			if (n >= PlayerProgress.GetPlayerProgress().m_tutorialsAlreadyShown.Length)
			{
				return;
			}
			PlayerProgress.GetPlayerProgress().m_tutorialsAlreadyShown [n] = state;
		}

		public void ShowTutorialMessageAfterADelay(float time, MessageID tutorialID)
		{
			StartCoroutine(ShowTutorialMessageAfterADelayCoroutine(time, tutorialID));
		}

		IEnumerator ShowTutorialMessageAfterADelayCoroutine(float time, MessageID tutorialID)
		{
			yield return new WaitForSeconds (time);
			ShowTutorialMessage (tutorialID);
		}

	    public void ShowTutorialMessage(MessageID tutorialID)
	    {
			if (!HasTutorialBeenShown (tutorialID)) 
			{
				for (int i = 0; i < m_tutorialRoots.Length; i++) 
				{
					m_tutorialRoots [i].SetActive (i == (int)tutorialID);
				}

				TutorialHasBeenShown(tutorialID);
				//Root.Instance.PauseGame (true);
				AudioManager.GetAudioManager().PlayAudioClip ("toggleOn");
			}
	    }

	    public void ClearAllTutorialMessages()
	    {
	        for (int i = 0; i < m_tutorialRoots.Length; i++)
	        {
	            m_tutorialRoots[i].SetActive(false);
	        }
	    }

		public void ClearedTutorialMessage()
		{
			AudioManager.GetAudioManager().PlayAudioClip ("toggleOff");
			//Root.Instance.PauseGame (false);
		}

	}
}
