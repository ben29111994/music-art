﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SWS;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityEngine.EventSystems;


namespace SonicBloom.Koreo
{
    public class SpawnItem : MonoBehaviour
    {
        public static SpawnItem SpawnItemInstance;
        public GameObject AudioBeat;
        public splineMove playerMove;
        public splineMove eventMove;
        public GameObject player;
        public GameObject[] environmentList;
        //public GameObject[] PoolObject;
        public GameObject Environment;
        public GameObject VolumetricLight;
        public List<GameObject> Obstacles;
        [EventID]
        public string eventID;
        public GameObject powerUp;
        public AudioSource audioCom;
        public AudioSource audioEvent;
        public static bool isMusicStart = false;
        float randomOffsetX = 0;
        float randomOffsetY = 0;
        public static bool isShake = false;
        public static float numberOfEvent = 0;
        public GameObject[] defaultShipPrefab;
        private GameObject shipModel;
        public GameObject pathList;
        string pathName;
        int tutorialStep = 0;
        bool isTutorial = false;
        public GameObject tutorialText;
        public Material[] rockCL;
        int randomTheme;
        public static int randomShip;
        int nextItem = 9;
        Color color;
        Color lightColor;
        public float maxSize = 1;
        public static bool isPlaying = false;
        public static bool isTrying = false;
        public TrailRenderer line;
        private Koreography koreography;
        private KoreographyTrackBase koreographyTrackBase;
        public List<KoreographyEvent> listEvent = new List<KoreographyEvent>();
        public GameObject Environment1;
        public GameObject Environment2;
        public GameObject Environment3;
        public GameObject Environment4;
        public GameObject Plane;
        int level;
        public List<GameObject> ArtPieces;
        public GameObject imagePiece;
        int artNumber;
        public static bool isShowOver = false;

        private void Awake()
        {
            artNumber = 0;
            SpawnItemInstance = this;
            if (isPlaying)
            {
                //Register the beat callback function
                var id = PlayerPrefs.GetInt("id");
                //Debug.Log("ID: " + id);
                //if (id > 15)
                //{
                //    AudioBeat.GetComponent<BeatDetection>().CallBackFunction = MyCallbackEventHandler;
                //}

                // create the default vehicle model
                randomShip = Random.Range(0, 3);
                SetSpawnShipModel(defaultShipPrefab[randomShip]);

                playerMove = player.GetComponent<splineMove>();
                eventMove = GetComponent<splineMove>();
            }
        }

        //public void MyCallbackEventHandler(BeatDetection.EventInfo eventInfo)
        //{
        //    switch (eventInfo.messageInfo)
        //    {
        //        case BeatDetection.EventType.Energy:
        //            SpawnEvent();
        //            break;
        //        case BeatDetection.EventType.HitHat:
        //            SpawnEvent();
        //            break;
        //        case BeatDetection.EventType.Kick:
        //            SpawnEvent();
        //            break;
        //        case BeatDetection.EventType.Snare:
        //            SpawnEvent();
        //            break;
        //    }
        //}
        public void CreatePieces(float gridX, float gridY, Texture image)
        {
            float w = 1 / gridX;
            float h = 1 / gridY;
            for(float i = 0; i < gridX; i++)
            {
                for(float j = 0; j < gridY; j++)
                {
                    GameObject artPiece = Instantiate(imagePiece) as GameObject;
                    artPiece.GetComponent<RawImage>().texture = image;
                    artPiece.GetComponent<RawImage>().uvRect = new Rect(w*i,h*j,w,h);
                    ArtPieces.Add(artPiece);
                }
            }
        }


        void SpawnEvent()
        {
            if (isTutorial)
            {
                randomOffsetX = -1;
                randomOffsetY = 1;
                StartCoroutine(waitForInput());
            }
            //var payloadValue = evt.GetIntValue();
            //Debug.Log(payloadValue);
            Vector3 spawnPosition = new Vector3(transform.position.x + randomOffsetX, transform.position.y + /*UnityEngine.Mathf.Abs(*/randomOffsetY/*)*/, transform.position.z);
            GameObject powerUpObject = Instantiate(powerUp, spawnPosition, transform.rotation) as GameObject;
            powerUpObject.transform.localScale = Vector3.zero;
            powerUpObject.transform.DOScale(maxSize, 3f);

            Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-20, 20), transform.position.y + Random.Range(-40, 40), transform.position.z + 50); ;
            float rangeValue = 0;
            //if (PlayerControl.isInvincible)
            //{
            //    rangeValue = Random.Range(-10, 10);
            //}
            //else
            //    rangeValue = Random.Range(-20, 20);
            Vector3 rot = transform.eulerAngles;
            rot.z = Random.Range(0, 360);
            rot.y = Random.Range(0, 360);
            rot.x = Random.Range(0, 360);
            for (int i = 0; i < 3; i++)
            {
                if (i % 2 == 0)
                {
                    rangeValue = 20;
                }
                else
                    rangeValue = -20;
                GameObject environmentSpawn = Instantiate(Environment, spawnPos, Quaternion.Euler(rot)) as GameObject;
                environmentSpawn.GetComponent<TrignometricMovement>().randomDistance = rangeValue;
                float size = Random.Range(1, 10);
                environmentSpawn.transform.DOScale(size/*0.05f*/, 1/*0.5f*/);
                environmentSpawn.transform.DOScale(5f/*0.05f*/, 15/*0.5f*/);
                Obstacles.Add(environmentSpawn);
            }
            for (int i = 9; i < environmentList.Length; i++)
            {
                if (i % 2 == 0)
                {
                    rangeValue = 20;
                }
                else
                    rangeValue = -20;
                GameObject stuffSpawn = Instantiate(environmentList[i], spawnPos, Quaternion.Euler(rot)) as GameObject;
                stuffSpawn.transform.DOScale(0.1f/*0.05f*/, 1/*0.5f*/);
                stuffSpawn.GetComponent<TrignometricMovement>().randomDistance = rangeValue;
            }
            StartCoroutine(delayBump());
            numberOfEvent++;
            Debug.Log(numberOfEvent);
        }

        // Use this for initialization
        public void SetSpawnShipModel(GameObject shipPrefab)
        {
            level = Random.Range(1, 5);
            if (level == 1)
            {
                Environment1.SetActive(true);
                Environment2.SetActive(true);
                Environment3.SetActive(false);
                Environment4.SetActive(false);
            }
            else if (level == 2)
            {
                Environment1.SetActive(false);
                Environment2.SetActive(false);
                Environment3.SetActive(false);
                Environment4.SetActive(false);
                Plane.SetActive(true);
            }
            else if (level == 3)
            {
                Environment1.SetActive(false);
                Environment2.SetActive(false);
                Environment3.SetActive(true);
                Environment4.SetActive(false);
            }
            else if (level == 4)
            {
                Environment1.SetActive(false);
                Environment2.SetActive(false);
                Environment3.SetActive(false);
                Environment4.SetActive(true);
            }
            //else if (level == 5)
            //{
            //    Environment1.SetActive(true);
            //    Environment2.SetActive(false);
            //    Environment3.SetActive(false);
            //    Environment4.SetActive(false);
            //}
            // destroy the current model if it exists
            //if (shipModel != null)
            //    Destroy(shipModel);

            //// instantiate a new vehicle model, and parent it to this object
            //Vector3 spawnPos = transform.position;
            //spawnPos.y = -0.5f;
            //shipModel = Instantiate(shipPrefab, spawnPos, transform.rotation) as GameObject;
            //shipModel.transform.parent = transform;
        }

        void Start()
        {
            var id = PlayerPrefs.GetInt("id");
            if (MenuManager.isHard)
            {
                line.enabled = false;
            }
            if (isPlaying)
            {
                isShake = false;
                numberOfEvent = 0;
                Obstacles.Clear();
                isMusicStart = false;
                pathName = pathList.name;
                eventMove.SetPath(WaypointManager.Paths[pathName]);
                var currentLife = PlayerPrefs.GetInt("life");
                currentLife--;
                PlayerPrefs.SetInt("life", currentLife);
                eventMove.onStart = true;
                eventMove.StartMove();
                Koreographer.Instance.RegisterForEvents(eventID, AddPowerUp);
                audioEvent.Play();
                StartCoroutine(delayStartEvent());
                koreography = Koreographer.Instance.GetKoreographyAtIndex(0);
                koreographyTrackBase = koreography.GetTrackByID(eventID);
                listEvent = koreographyTrackBase.GetAllEvents();
            }
            if (GetArtData.Instance.imageList.Count > 0)
            {
                CreatePieces(GetArtData.Instance.gridListX[artNumber], GetArtData.Instance.gridListY[artNumber], GetArtData.Instance.imageList[artNumber]);
                if (artNumber < GetArtData.Instance.imageList.Count - 1)
                {
                    artNumber++;
                }
            }
        }

        private void FixedUpdate()
        {           
            if (!audioCom.isPlaying && isMusicStart && !isTutorial && !isShowOver)
            {
                PlayerControl.PlayerControlInstance.OnFinishGame();
            }
            //Debug.Log(numberOfEvent);
            if (numberOfEvent >= 12)
            {
                isShake = true;
            }
            else
            {
                isShake = false;
            }
        }

        IEnumerator delaySpawnLight()
        {
            yield return new WaitForSeconds(1);
            float randomOffset = Random.Range(800, 1000);
            Vector3 spawnPosition = new Vector3(transform.position.x + randomOffset, transform.position.y, transform.position.z + randomOffset);
            GameObject lightObject = Instantiate(VolumetricLight, spawnPosition, VolumetricLight.transform.rotation) as GameObject;
            lightObject.transform.localScale = Vector3.zero;
            lightObject.transform.DOScale(1f, 1f);
            StartCoroutine(delaySpawnLight());
        }

        IEnumerator delayStartEvent()
        {
            yield return new WaitForSeconds(5f);
            playerMove.SetPath(WaypointManager.Paths[pathName]);
            playerMove.onStart = true;
            playerMove.StartMove();
            var id = PlayerPrefs.GetInt("id");
            audioCom.Play();
            isMusicStart = true;
            StartCoroutine(changeOffSet());
            yield return new WaitForSeconds(1f);
            PlayerControl.isFollowPlayer = true;
            PlayerControl.PlayerControlInstance.isCutscene = false;
        }

        IEnumerator changeSpeedOverTime()
        {
            yield return new WaitForSeconds(1f);
            if (eventMove.speed >= 17.5f)
            {
                eventMove.ChangeSpeed(eventMove.speed -= 0.04f);
                eventMove.speed -= 0.04f;
            }
            StartCoroutine(changeSpeedOverTime());
        }

        IEnumerator changeOffSet()
        {
            yield return new WaitForSeconds(0.5f);
            if (randomOffsetX >= 1)
            {
                randomOffsetX = Random.Range(randomOffsetX - 1f, randomOffsetX);
            }
            else if (randomOffsetX <= -1)
            {
                randomOffsetX = Random.Range(randomOffsetX, randomOffsetX + 1f);
            }
            else
            {
                randomOffsetX = Random.Range(randomOffsetX - 1f, randomOffsetX + 1f);
            }

            if (randomOffsetY >= 1)
            {
                randomOffsetY = Random.Range(randomOffsetY - 1f, randomOffsetY);
            }
            else if (randomOffsetY <= -1)
            {
                randomOffsetY = Random.Range(randomOffsetY, randomOffsetY + 1f);
            }
            else
            {
                randomOffsetY = Random.Range(randomOffsetY - 1f, randomOffsetY + 1f);
            }
            StartCoroutine(changeOffSet());
        }

        void AddPowerUp(KoreographyEvent evt)
        {
            Vector3 spawnPosition = new Vector3(transform.position.x + randomOffsetX, transform.position.y + /*UnityEngine.Mathf.Abs(*/randomOffsetY/*)*/, transform.position.z);
            //GameObject powerUpObject = Instantiate(powerUp, spawnPosition, transform.rotation) as GameObject;
            //powerUpObject.transform.localScale = Vector3.zero;
            //powerUpObject.transform.DOScale(maxSize, 3f);
            if (ArtPieces.Count > 0)
            {
                var randomIndex = Random.Range(0, ArtPieces.Count - 1);
                ArtPieces[randomIndex].transform.position = spawnPosition;
                ArtPieces[randomIndex].transform.localScale = Vector3.zero;
                ArtPieces[randomIndex].transform.DOScale(1, 1f);
                ArtPieces.RemoveAt(randomIndex);
            }
            else
            {
                CreatePieces(GetArtData.Instance.gridListX[artNumber], GetArtData.Instance.gridListY[artNumber], GetArtData.Instance.imageList[artNumber]);
                artNumber++;
            }

            //Level 1
            if (level == 1)
            {
                Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-10, 10), transform.position.y + Random.Range(-20, 20), transform.position.z + 50); ;
                float rangeValue = 5;
                Vector3 rot = transform.eulerAngles;
                rot.z = Random.Range(0, 360);
                rot.y = /*Random.Range(0, 360);*/0;
                rot.x = /*Random.Range(0, 360);*/0;

                for (int i = 13; i < 16; i++)
                {
                    if (i > 13)
                    {
                        GameObject stuffSpawn = Instantiate(environmentList[i], spawnPos, Quaternion.EulerRotation(rot)) as GameObject;
                        stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 30), 5);
                        stuffSpawn.GetComponent<TrignometricMovement>().randomDistance = 20;
                    }
                    else
                    {
                        GameObject stuffSpawn = Instantiate(environmentList[i], spawnPos, Quaternion.EulerRotation(rot)) as GameObject;
                        stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 255), 3);
                        stuffSpawn.GetComponent<TrignometricMovement>().randomDistance = rangeValue;
                    }
                }
            }

            //Level 2
            else if (level == 2)
            {
                Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-30, 30), transform.position.y - 120, transform.position.z + 100);
                Vector3 spawnPos1 = new Vector3(spawnPos.x, -10f, PlayerControl.PlayerControlInstance.transform.position.z);
                float rangeValue = 0;
                //for (int i = 13; i < environmentList.Length; i++)
                //{
                GameObject stuffSpawn = Instantiate(environmentList[17], spawnPos, Quaternion.identity) as GameObject;
                stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 200), 1);
                stuffSpawn.transform.localScale = new Vector3(0.1f, 1f, 0.1f);
                //stuffSpawn.transform.DOScale(new Vector3(0.1f, 500, 0.1f), 20);
                iTween.ScaleTo(stuffSpawn, new Vector3(0.1f, 155, 0.1f), 10);

                GameObject stuffSpawn1 = Instantiate(environmentList[18], spawnPos1, Quaternion.Euler(new Vector3(90, 0, 0))) as GameObject;
                stuffSpawn1.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 200), 1);
                stuffSpawn1.transform.localScale = new Vector3(0.1f, 1f, 0.1f);
                //stuffSpawn1.transform.DOScale(new Vector3(0.1f, 120, 0.1f), 4f);
                iTween.ScaleTo(stuffSpawn1, new Vector3(0.1f, 152, 0.1f), 4);
                StartCoroutine(delayDestroy(stuffSpawn1));
                //}
            }

            //Level 3
            else if (level == 3)
            {
                Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-10, 10), transform.position.y + Random.Range(-20, 20), transform.position.z + 50); ;
                float rangeValue = 5;
                Vector3 rot = transform.eulerAngles;
                rot.z = Random.Range(0, 360);
                rot.y = Random.Range(0, 360);
                rot.x = Random.Range(0, 360);

                int i = Random.Range(0, 9);
                GameObject stuffSpawn = Instantiate(environmentList[i], spawnPos, Quaternion.EulerRotation(rot)) as GameObject;
                stuffSpawn.transform.DOScale(new Vector3(1, 1, 1), 1);
                stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 30), 5);
                stuffSpawn.GetComponent<TrignometricMovement>().randomDistance = 20;

                StartCoroutine(delayBump());
                numberOfEvent++;
            }

            //Level 4
            else if (level == 4)
            {
                Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-5, 5), transform.position.y + Random.Range(-5, 5), transform.position.z + 25); 
                float rangeValue = 5;
                Vector3 rot = transform.eulerAngles;
                rot.z = Random.Range(0, 360);

                GameObject stuffSpawn = Instantiate(environmentList[19], spawnPos, Quaternion.EulerRotation(rot)) as GameObject;
                stuffSpawn.transform.DOScale(20, 1);
                stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 255), 3);
            }

            //Level 5
            //else if (level == 5)
            //{
            //    Vector3 spawnPos = new Vector3(transform.position.x + Random.Range(-5, 5), transform.position.y + Random.Range(-5, 5), transform.position.z + 50); ;
            //    float rangeValue = 5;
            //    Vector3 rot = transform.eulerAngles;
            //    rot.z = Random.Range(0, 360);

            //    var randomSpawn = Random.Range(1, 4);
            //    if(randomSpawn == 2)
            //    {
            //        GameObject stuffSpawn = Instantiate(environmentList[20], spawnPos, Quaternion.EulerRotation(rot)) as GameObject;
            //        stuffSpawn.GetComponent<SpriteRenderer>().DOColor(new Color32(255, 255, 255, 255), 3);
            //        stuffSpawn.transform.DOScale(1.5f, 1);
            //        //stuffSpawn.GetComponent<TrignometricMovement>().randomDistance = 0;
            //    }
            //}
            //Debug.Log(numberOfEvent);
        }

        IEnumerator delayDestroy(GameObject stuffSpawn1)
        {
            yield return new WaitForSeconds(30);
            Destroy(stuffSpawn1);
        }

        IEnumerator delayBump()
        {
            yield return new WaitForSeconds(3);
            foreach(var item in Obstacles)
            {
                if (item != null)
                {
                    item.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0.5f), 0.5f);
                }
            }
            //Debug.Log(Obstacles.Count);
        }

        IEnumerator waitForInput()
        {
            yield return new WaitForSeconds(2.8f);
            if (tutorialStep < 2)
            {
                tutorialText.SetActive(true);
                if(tutorialStep == 1)
                {
                    randomOffsetX = 0;
                    randomOffsetY = 0;
                    tutorialText.GetComponent<Text>().text = "TO GET PERFECT PERFORM, HIT THE RED ITEM IN THE MIDDLE OF THE BLUE CIRCLE"; 
                }
                else
                    tutorialText.GetComponent<Text>().text = "SWIPE IN ANY DIRECTION TO MOVE AND HIT THE BLUE CIRCLE, YOU CAN ALSO HOLD AND SWIPE";
                tutorialStep++;
                audioCom.Pause();
                Time.timeScale = 0;
                audioCom.Pause();
            }
            else
                isTutorial = false;
        }

        public void ButtonTest()
        {
            var currentScale = maxSize;
            if (currentScale >= 1f)
            {
                currentScale = 0.2f;
            }
            else
            {
                currentScale += 0.1f;
            }
            maxSize = currentScale;
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            button.GetComponent<Text>().text = "Size:" + currentScale;
        }
    }
}
