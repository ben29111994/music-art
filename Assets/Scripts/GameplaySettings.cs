using System;
using UnityEngine;
using UnityEngine.UI;

namespace SonicBloom.Koreo
{
    public class GameplaySettings
    {
		public GameplaySettings ShallowCopy()
		{
			return (GameplaySettings) this.MemberwiseClone();
		}
    }
}
