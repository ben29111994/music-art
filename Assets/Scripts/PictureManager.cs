using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace SonicBloom.Koreo
{
	public delegate void CallBack();

	public class PictureManager : MonoBehaviour
    {
		public Texture2D[] textureList;
		public Dictionary<string, Texture2D> textureDictionary = new Dictionary<string, Texture2D>();


		public static PictureManager m_instance = null;


		public static PictureManager Instance
		{
			get
			{
				return m_instance;
			}
		}

		// Use this for initialization
		private void Start()
		{
			m_instance = this;
		}


		public void LoadAllSpecifiedTextures (string[] textureNames, string pictureSubDirectory, CallBack finishedCallback)
		{
			StartCoroutine(LoadTextures(textureNames, pictureSubDirectory, finishedCallback));
		}


        IEnumerator LoadTextures(string[] fileNames, string pictureSubDirectory, CallBack finishedCallback)
        {
            textureList = new Texture2D[fileNames.Length];

            int index = 0;
            foreach (string name in fileNames)
            {
                if (textureDictionary.ContainsKey(name))
                {
                    // we've already loaded that texture. Use that instance.
                    textureList[index++] = textureDictionary[name];
                }
                else
                {
                    Texture2D texTmp = new Texture2D(4, 4, TextureFormat.PVRTC_RGB4, false);
                    string last4chars = 4 > name.Length ? name : name.Substring(name.Length - 4);
                    if ((last4chars.ToUpper() != ".MOV") && (last4chars.ToUpper() != ".M4V"))
                    {
                        string streamingAssetsPath = Application.streamingAssetsPath;
                        if (pictureSubDirectory != "")
                            streamingAssetsPath = streamingAssetsPath + "/" + pictureSubDirectory;

                        string fullPathName = System.IO.Path.Combine(streamingAssetsPath, name);

                        if (!fullPathName.Contains("://"))
                        {
                            fullPathName = "file://" + fullPathName;
                        }

                        Debug.Log("fullPathName : " + fullPathName);
                        WWW www = new WWW(fullPathName);

                        yield return www;

                        www.LoadImageIntoTexture(texTmp);
                    }

                    textureList[index++] = texTmp;
                    textureDictionary[name] = texTmp;

                    if (!SpawnItem.isPlaying && !MenuManager.isLoadCollection)
                    {
                        string[] info = name.Split('-');
                        string[] artist = info[0].Split('/');
                        Debug.Log(info[0] + "|" + info[1] + "|" + info[2]);
                        var artData = Instantiate(UIManager.instance.artCollection);
                        artData.transform.parent = UIManager.instance.artCollectionContent.transform;
                        artData.transform.localScale = new Vector3(1, 1, 1);
                        artData.gameObject.transform.GetChild(0).GetComponent<Text>().text = artist[1];
                        artData.gameObject.transform.GetChild(1).GetComponent<Text>().text = info[1];
                        artData.gameObject.transform.GetChild(2).GetComponent<Text>().text = info[2].Replace(".jpg", "");
                        artData.gameObject.transform.GetChild(3).GetComponent<RawImage>().texture = texTmp;
                        string id = artist[0];
                        var unlockStatus = PlayerPrefs.GetInt(id + "unlock");
                        if (unlockStatus == 1 || id == "1")
                        {

                        }
                        else
                        {
                            artData.gameObject.transform.GetChild(4).transform.gameObject.SetActive(false);
                            artData.GetComponent<Button>().interactable = false;
                        }
                    }
                }
            }

            yield return new WaitForSeconds(0.01f); // avoids immediate callback in the case that there are no textures to load.

            // notify the game that we are finished loading textures
            if (UIManager.instance.loadingPanel != null)
            {
                StartCoroutine(delayLoadingPanel());
            }
			finishedCallback();
		}

        IEnumerator delayLoadingPanel()
        {
            UIManager.instance.loadingPanel.GetComponent<Image>().DOColor(new Color32(0, 0, 0, 0), 1);
            //var childObject = UIManager.instance.loadingPanel.transform.GetChild(0);
            //childObject.GetComponent<Image>().DOColor(new Color32(0, 0, 0, 0), 1);
            yield return new WaitForSeconds(1);
            if(UIManager.instance.loadingPanel != null)
            UIManager.instance.loadingPanel.SetActive(false);
        }

		public Texture2D GetTexture(int index)
		{
			Debug.Log ("number of textures = "+textureList.Length);
			Debug.Log ("textures = "+textureList[index]);

			return textureList[index];
		}

		public Texture2D GetTextureByName(string name)
		{
			return textureDictionary[name];
		}

		public int GetNumberOfTextures()
		{
			return textureList.Length;
		}

        public static PictureManager GetThumbnailManager()
        {
            // tries to find question manager in the scene
            // if not, then create one and initialise it.
            GameObject thumbnailManagerObject = GameObject.Find("ThumbnailManagerPrefab(Clone)");
            if (thumbnailManagerObject)
            {
                // return the existing one
                return thumbnailManagerObject.GetComponent<PictureManager>();
            }
            else
            {
                // create a new one
                thumbnailManagerObject = Instantiate(Resources.Load("ThumbnailManagerPrefab")) as GameObject;
                PictureManager thumbnailManager = thumbnailManagerObject.GetComponent<PictureManager>();
                DontDestroyOnLoad(thumbnailManager.gameObject);  // make it persist between scenes
                return thumbnailManager;
            }
        }
    }
}
