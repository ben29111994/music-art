﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;
using UnityEngine.SceneManagement;

public class LevelSelectController : MonoBehaviour 
{
	//[SerializeField] 
	//GameObject m_groupScrollView;
	//[SerializeField] 
	public LevelGroupContent m_levelGroupContent;
	[SerializeField] 
	GameObject m_questionScrollView;
    [SerializeField]
    LevelSelectContent m_questionLevelSelectContent;

    private QuestionManager m_questionManager;
    private AudioManager m_audioManager;
    private SettingsManager m_settingsManager;

    private PictureManager m_thumbnailManager;
    private PlayerProgress m_playerProgress;

    int m_screenWidth;
	Vector2 m_initialScrollViewPos;
    bool m_showingGroupView = false;

	public static LevelSelectController m_instance;

	const float TRANSIT_TIME = 0.2f;

    public GameObject gameplayCanvas;
    public GameObject gameManager;
    public GameObject imageCover;
    public bool isLoaded = false;


    public static LevelSelectController Instance
	{
		get
		{
			return m_instance;
		}
	}

	LevelSelectController()
	{
		m_instance = this;
	}

    private void Awake()
    {
        m_questionManager = QuestionManager.GetQuestionManager();
        m_audioManager = AudioManager.GetAudioManager();
        m_settingsManager = SettingsManager.GetSettingsManager();
        m_thumbnailManager = PictureManager.GetThumbnailManager();
        m_settingsManager.ResetSettings();
        m_questionManager.LoadQuestions(LoadThumbnails);
    }

    void Start()
	{
        isLoaded = false;
        //m_initialScrollViewPos = m_groupScrollView.transform.localPosition;
        m_screenWidth = 1242;       // this is the reference resolution x-value used in canvas scaler
        Debug.Log("ScreenWidth = " + m_screenWidth);
        m_questionScrollView.transform.localPosition = new Vector2(m_screenWidth, m_initialScrollViewPos.y);
        m_levelGroupContent.Init();
        TransitToGroupView();
    }

    void LoadThumbnails()
    {
        m_playerProgress = PlayerProgress.GetPlayerProgress();

        // load all the thumbnails
        string[] thumbnails = m_questionManager.AllThumbnailNamesInAllQuizzes();
        CallBack finishedLoadingTexturesCallback = FinishedLoadingTextures;
        m_thumbnailManager.LoadAllSpecifiedTextures(thumbnails, "", FinishedLoadingTextures);
    }

    void FinishedLoadingTextures()
    {
        //m_loadingText.SetActive(false);
        //m_startButton.SetActive(true);
    }

    public void TransitToGroupView()
	{
		//m_groupScrollView.SetActive(true);
		m_questionScrollView.SetActive(true);
		//m_groupScrollView.GetComponent<SonicBloom.Koreo.Movement>().MoveTo(new Vector2(0f, m_initialScrollViewPos.y),TRANSIT_TIME);
		m_questionScrollView.GetComponent<SonicBloom.Koreo.Movement>().MoveTo(new Vector2(m_screenWidth, m_initialScrollViewPos.y),TRANSIT_TIME);
        m_showingGroupView = true;
        isLoaded = true;
    }

    public void TransitToQuestionView(int numOfAnsweredQuestions, string quizName)
    {
        //m_groupScrollView.SetActive(true);
        m_questionScrollView.SetActive(true);
        //m_groupScrollView.GetComponent<SonicBloom.Koreo.Movement>().MoveTo(new Vector2(-m_screenWidth, m_initialScrollViewPos.y), TRANSIT_TIME);
        m_questionScrollView.GetComponent<SonicBloom.Koreo.Movement>().MoveTo(new Vector2(0f, m_initialScrollViewPos.y), TRANSIT_TIME);

        StartCoroutine(GetScrollPosAfterASingleFrame(numOfAnsweredQuestions));
    }

    IEnumerator GetScrollPosAfterASingleFrame(int numOfAnsweredQuestions)
    {
        yield return null;

        // position the scroll view at the next unanswered question
        Vector2 focusPos = m_questionLevelSelectContent.GetScrollPosForLevelButton(numOfAnsweredQuestions);

        RectTransform rt = m_questionLevelSelectContent.gameObject.GetComponent<RectTransform>();
        Vector2 pos = rt.anchoredPosition;
        pos.y = focusPos.y;
        rt.anchoredPosition = pos;

        m_showingGroupView = false;
    }

	public void PressedBackButton()
	{
		AudioManager.GetAudioManager ().PlayAudioClip ("back");
        if (SpawnItem.isPlaying)
        {
            //SceneManager.LoadScene("Main");
            PlayerControl.PlayerControlInstance.OnGameOver();
        }
        else
        {
            TransitToGroupView();
        }
	}

    public void BackToAnotherArt()
    {
        AudioManager.GetAudioManager().PlayAudioClip("back");
        gameplayCanvas.SetActive(false);
        gameManager.SetActive(false);
        imageCover.SetActive(false);
    }
}
