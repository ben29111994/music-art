﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using SonicBloom.Koreo;

public delegate void LevelButtonCallback(int levelNumber);

public class LevelButton : MonoBehaviour {

    [SerializeField]
    Text m_artworkNumber;
    [SerializeField]
    Text m_artistName;
    [SerializeField]
    Text m_artworkName;
    [SerializeField]
    Text m_artworkDate;
    [SerializeField]
    Text m_quizName;
    [SerializeField] 
	Text m_levelCompletedText;
    [SerializeField]
    GameObject m_levelCompletedIcon;
    [SerializeField]
    GameObject m_frame;
    [SerializeField] 
	GameObject m_levelIcon;
    [SerializeField]
    GameObject m_levelThumbnail;
    [SerializeField] 
	Button m_levelButton;
    [SerializeField]
    Texture m_lockedTexture;
    [SerializeField]
    Texture m_lockedButPlayableTexture;
    [SerializeField]
    Texture m_unlockedTexture;
    [SerializeField]
    Texture m_quizIconTexture;
	[SerializeField]
	RawImage m_quizBanner;
	[SerializeField]
	GameObject grayscaleIcon;
    public bool isFinished = false;

    public int m_levelID;

	RawImage m_levelThumbnailImage;

    public enum IconImages
    {
        QUESTION_LOCKED = 0,
        QUESTION_UNLOCKED,
        QUESTION_LOCKED_BUT_PLAYABLE,
        QUIZ
    }

	private LevelButtonCallback m_levelButtonCallback;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void QuestionInit(int levelNumber, int levelID, string artistName, string artworkName, string artworkDate, string thumbnail, LevelButtonCallback callback, bool isActive)
	{
        m_artistName.text = artistName;
        m_artworkName.text = artworkName;
        m_artworkDate.text = artworkDate;
        m_quizName.gameObject.SetActive(false);
        m_levelID = levelID;
		SetSelectCallback(callback);
		m_levelButton.interactable = isActive;
        m_levelThumbnailImage = m_levelThumbnail.GetComponent<RawImage> ();

        if (thumbnail != null)
        {
            Texture texture = PictureManager.GetThumbnailManager().GetTextureByName(thumbnail);
            m_levelThumbnailImage.texture = texture;
            m_levelThumbnail.SetActive(true);
        }
        else
        {
            m_levelThumbnail.SetActive(false);
        }
    }

    public void QuizInit(int levelNumber, int levelID, string quizName, string thumbnail, LevelButtonCallback callback, bool isActive)
    {
        //m_artworkNumber.text = name1;
        m_quizName.text = quizName;
        m_artistName.gameObject.SetActive(false);
        m_artworkName.gameObject.SetActive(false);
        m_artworkDate.gameObject.SetActive(false);
        m_levelID = levelID;
        SetSelectCallback(callback);
        m_levelButton.interactable = isActive;
        m_levelThumbnailImage = m_levelThumbnail.GetComponent<RawImage>();

        if (thumbnail != null)
        {
            Texture texture = PictureManager.GetThumbnailManager().GetTextureByName(thumbnail);
            m_levelThumbnailImage.texture = texture;
            m_levelThumbnail.SetActive(true);
        }
        else
        {
            m_levelThumbnail.SetActive(false);
        }
    }

    public void SetIconImage(IconImages imageID)
    {
        RawImage raw = m_levelIcon.GetComponent<RawImage>();
        switch (imageID)
        {
            case IconImages.QUESTION_LOCKED_BUT_PLAYABLE:
                raw.texture = m_lockedButPlayableTexture;
//                m_levelIcon.SetActive(true);
                m_levelThumbnail.SetActive(true);
                break;
            case IconImages.QUESTION_LOCKED:
                raw.texture = m_lockedTexture;
                // change these to false to show no icon on locked questions
//                m_levelIcon.SetActive(true);
	            //Grayscale icon
	            grayscaleIcon.SetActive(true);
                m_levelThumbnail.SetActive(true);
                break;
            case IconImages.QUESTION_UNLOCKED:
                raw.texture = m_unlockedTexture;
//                m_levelIcon.SetActive(true);
                m_levelThumbnail.SetActive(true);
                break;
            case IconImages.QUIZ:
                raw.texture = m_quizIconTexture;
//                m_levelIcon.SetActive(true);
                m_levelThumbnail.SetActive(false);
                break;
        }
    }

	public void PressedButton(bool isPlaySound)
	{
        if (isPlaySound)
        {
            AudioManager.GetAudioManager().PlayAudioClip("buttonClick");
            MenuManager.MenuManagerInstance.delayShowArtData();
        }

		Debug.Log("PressedButton = "+m_levelID);
		m_levelButtonCallback(m_levelID);
    }

	public void SetCompletedCount(int completed, int outOf)
	{
		if (completed == outOf)
		{
			m_levelCompletedText.gameObject.SetActive(false);
			m_levelCompletedIcon.SetActive(true);
            m_levelThumbnail.GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
            isFinished = true;
        }
		else
		{
			m_levelCompletedText.gameObject.SetActive(true);
			m_levelCompletedText.text = completed.ToString()+"/"+outOf.ToString();
			m_levelCompletedIcon.SetActive(false);
            isFinished = false;
		}
	}

	public void HideCompletionCount(bool hide)
	{
		m_levelCompletedText.gameObject.SetActive(!hide);
		m_levelCompletedIcon.SetActive(!hide);
	}

	public void SetSelectCallback(LevelButtonCallback callback)
	{
		m_levelButtonCallback = callback;
	}

	public void SetContentAlpha(float alpha)
	{
        Color c = m_levelCompletedText.color;
		c.a = alpha;
		m_levelCompletedText.color = c;

        c = m_artworkNumber.color;
        c.a = alpha;
        m_artworkNumber.color = c;

        c = m_artworkName.color;
        c.a = alpha;
        m_artworkName.color = c;

        c = m_artworkDate.color;
        c.a = alpha;
        m_artworkDate.color = c;

        c = m_artistName.color;
        c.a = alpha;
        m_artistName.color = c;

        c = m_quizName.color;
        c.a = alpha;
        m_quizName.color = c;

        c = m_frame.GetComponent<Image>().color;
        c.a = alpha;
        m_frame.GetComponent<Image>().color = c;

		c = m_levelCompletedIcon.GetComponent<RawImage>().color;
		c.a = alpha;
		m_levelCompletedIcon.GetComponent<RawImage>().color = c;

		c = m_quizBanner.GetComponent<RawImage>().color;
		c.a = alpha;
		m_quizBanner.GetComponent<RawImage>().color = c;

		//c = m_levelIcon.GetComponent<RawImage>().color;
		//c.a = alpha;
		//m_levelIcon.GetComponent<RawImage>().color = c;
	}

	public void SetRoomImage(Texture image)
	{
		m_quizBanner.texture = image;
		m_quizBanner.transform.parent.gameObject.SetActive (true);
	}
}
