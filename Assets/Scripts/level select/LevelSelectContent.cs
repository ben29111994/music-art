﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;
using UnityEngine.SceneManagement;

public class LevelSelectButtonData
{
    public int m_levelNum;
    public string m_artistName;
    public string m_artworkName;
    public string m_artworkDate;
    public string m_quizName;
    public string m_thumbnail;
	public int m_levelID;
	public bool m_isLevel;
	public bool m_isButtonActive;
	public LevelButtonCallback m_callback;
	public int m_completionScore;
	public int m_maxCompletion;

    public LevelSelectButtonData()
    {
    }

    public void InitForQuestion(int levelNum, LevelButtonCallback callback, string thumbnail, string artistName = "", string artworkName = "", string artworkDate = "", bool isButtonActive = true)
	{
		m_levelNum = levelNum;
        m_artistName = artistName;
        m_artworkName = artworkName;
        m_artworkDate = artworkDate;
        m_thumbnail = thumbnail;
		m_callback = callback;
		m_isLevel = false;
		m_isButtonActive = isButtonActive;
	}

    public void InitForQuiz(int levelNum, LevelButtonCallback callback, string thumbnail, string quizName = "", bool isButtonActive = true)
    {
        m_levelNum = levelNum;
        m_quizName = quizName;
        m_thumbnail = thumbnail;
        m_callback = callback;
        m_isLevel = true;
        m_isButtonActive = isButtonActive;
    }

    public void SetCompletion(int score, int outOf)
	{
		m_completionScore = score;
		m_maxCompletion = outOf;
	}
}

public class LevelSelectContent : MonoBehaviour {

	[SerializeField] 
	GameObject m_buttonPrefab;
	[SerializeField] 
	Text m_sectionHeader;
    [SerializeField]
    GameObject m_unlockedRoomRewardSequence;
	[SerializeField]
	Texture[] m_roomBannerTextures;
    [SerializeField]
    Texture m_hiddenRoomTexture;

    LevelButton[] m_levelButtons;

    // Use this for initialization
    void Start () 
	{
//		//AGTEMP - just use this data for the init for now
//		List<LevelSelectButtonData> testData = new List<LevelSelectButtonData>();
//		for(int i=1; i<20; i++)
//		{
//			testData.Add(new LevelSelectButtonData(i));
//		}
//
//		Init(testData);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

    public Vector2 GetScrollPosForLevelButton(int n)
    {
        const int NUM_VISIBLE_BUTTONS = 5;

        int centreOffset = (NUM_VISIBLE_BUTTONS + 1) / 2;

        n = n - centreOffset;

        if (n > (m_levelButtons.Length - centreOffset))
        {
            n = m_levelButtons.Length - centreOffset;
        }
        if (n < 0)
        {
            n = 0;
        }

        GameObject gOb = m_levelButtons[n].gameObject;
        RectTransform rt = gOb.GetComponent<RectTransform>();
        Vector2 size = rt.sizeDelta;

        return new Vector2(0f, n * size.y);
    }

	public bool Init(List<LevelSelectButtonData> buttonData, bool groupIsActive = true)
	{
        Debug.Log("**** level select content init ****");
        ClearExistingButtons();

        m_levelButtons = new LevelButton[buttonData.Count];

        bool allComplete = true;
        int number = 1;
		foreach(LevelSelectButtonData lbd in buttonData)
		{
			GameObject button = Instantiate(m_buttonPrefab, transform);
			LevelButton levelButton = button.GetComponent<LevelButton>();
            m_levelButtons[number-1] = levelButton;
			//levelButton.Init(number++, lbd.m_levelNum, lbd.m_name1, lbd.m_name2, lbd.m_thumbnail, lbd.m_callback, lbd.m_isButtonActive);
			if (!lbd.m_isLevel)
			{
                levelButton.QuestionInit(number++, lbd.m_levelNum, lbd.m_artistName, lbd.m_artworkName, lbd.m_artworkDate, lbd.m_thumbnail, lbd.m_callback, true/*lbd.m_isButtonActive*/);
                // this is a button for a question - show it as either completed or not
                if (lbd.m_completionScore > 0)
				{
					levelButton.HideCompletionCount(false);				
					levelButton.SetCompletedCount(1, 1);
                    levelButton.SetIconImage(LevelButton.IconImages.QUESTION_UNLOCKED);
				}
				else
				{
					levelButton.HideCompletionCount(true);
                    if (lbd.m_isButtonActive)
                        levelButton.SetIconImage(LevelButton.IconImages./*QUESTION_LOCKED_BUT_PLAYABLE*/QUESTION_UNLOCKED);
                    else
                        levelButton.SetIconImage(LevelButton.IconImages./*QUESTION_LOCKED*/QUESTION_UNLOCKED);
                }

                allComplete = false;    // this is a question - allComplete is for the quiz selection screen
            }
			else
			{
                levelButton.QuizInit(number, lbd.m_levelNum, lbd.m_quizName, lbd.m_thumbnail, lbd.m_callback, true/*lbd.m_isButtonActive*/);
                levelButton.SetIconImage(LevelButton.IconImages.QUIZ);
                //if (groupIsActive)
    				//levelButton.SetRoomImage (m_roomBannerTextures[lbd.m_levelNum]);
                //else
                //    levelButton.SetRoomImage(/*m_hiddenRoomTexture*/m_roomBannerTextures[lbd.m_levelNum]);
                number++;
                // this is a button for a quiz. Show the number of questions completed.
                levelButton.SetCompletedCount(lbd.m_completionScore, lbd.m_maxCompletion);
				//if (lbd.m_completionScore == 0 && lbd.m_isButtonActive && (lbd.m_levelNum != 0))
    //            {
    //                // we've only just unlocked it. Flash it.
    //                SonicBloom.Koreo.Visibility vis = button.GetComponentInChildren<SonicBloom.Koreo.Visibility>(true);
    //                vis.Flash(15, 0.1f, 0.1f, true);

				//	GameObject canvas = GameObject.Find ("Canvas"); 
				//	Instantiate(m_unlockedRoomRewardSequence, Vector3.zero, Quaternion.identity, canvas.transform);
    //            }
				if (lbd.m_completionScore < lbd.m_maxCompletion)
				{
					allComplete = false;
				}
            }
        }

		return allComplete;
	}

	void ClearExistingButtons()
	{
		LevelButton[] buttons = transform.GetComponentsInChildren<LevelButton>();
		foreach(LevelButton b in buttons)
		{
			b.transform.SetParent(null);
			Destroy(b.gameObject);
		}
	}

	public void SetHeaderText(string text)
	{
		m_sectionHeader.text = text.ToUpper();
	}


	public void SetContentAlpha(float alpha, int numberOfButtons = 0)
	{
        if (m_sectionHeader != null)
        {
            Color c = m_sectionHeader.color;
			c.a = (numberOfButtons > 0) ? 1f : alpha;
            m_sectionHeader.color = c;
        }

        int count = 0;
		LevelButton[] buttons = transform.GetComponentsInChildren<LevelButton>();
		foreach(LevelButton b in buttons)
		{
            b.SetContentAlpha((count < numberOfButtons) ? 1f : alpha);

            count++;
		}
	}
}
