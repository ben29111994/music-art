﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SonicBloom.Koreo
{
    public class GetArtData : MonoSingleton<GetArtData>
    {
        public static GetArtData GetArtDataInstance;
        public List<Texture> imageList;
        public List<float> gridListX;
        public List<float> gridListY;
        //public GameObject inGameQuestionScrollView;
        public GameObject spawnArtGamePlay;
        public GameObject Menu;
        public List<GameObject> artGamePlayObjects;
        public int maxArt;
        // Use this for initialization
        public void AddData(GameObject data)
        {
            imageList.Add(data.GetComponent<RawImage>().texture);
        }

        public void AddGridData(float gridX, float gridY)
        {
            gridListX.Add(gridX);
            gridListY.Add(gridY);
        }

        public void OnLevelWasLoaded(int level)
        {
            if (!SpawnItem.isPlaying)
            {
                imageList.Clear();
                gridListX.Clear();
                gridListY.Clear();
                artGamePlayObjects.Clear();
            }
            foreach (var item in artGamePlayObjects)
            {
                if(item == null)
                {
                    artGamePlayObjects.Remove(item);
                }
            }
            for (int i = 0; i < 3; i++)
            {
                spawnArtGamePlay = this.gameObject.transform.GetChild(0).gameObject;
                if (spawnArtGamePlay != null)
                {
                    if (spawnArtGamePlay.name == "Menu")
                    {
                        Menu = spawnArtGamePlay;
                    }
                    artGamePlayObjects.Add(spawnArtGamePlay);
                    spawnArtGamePlay.transform.parent = MenuManager.MenuManagerInstance.transform;
                    spawnArtGamePlay.transform.parent = null;
                }
            }
        }
    }
}
