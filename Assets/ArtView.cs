﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace SonicBloom.Koreo
{
    public class ArtView : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        public void OnArtCollectionClick()
        {
            UIManager.instance.viewArt.SetActive(true);
            var button = EventSystem.current.currentSelectedGameObject.gameObject;
            UIManager.instance.viewArt.gameObject.transform.GetChild(0).GetComponent<Text>().text = button.gameObject.transform.GetChild(0).GetComponent<Text>().text;
            UIManager.instance.viewArt.gameObject.transform.GetChild(1).GetComponent<Text>().text = button.gameObject.transform.GetChild(1).GetComponent<Text>().text;
            UIManager.instance.viewArt.gameObject.transform.GetChild(2).GetComponent<Text>().text = button.gameObject.transform.GetChild(2).GetComponent<Text>().text;
            UIManager.instance.viewArt.gameObject.transform.GetChild(3).GetComponent<RawImage>().texture = button.gameObject.transform.GetChild(3).GetComponent<RawImage>().texture;
            UIManager.instance.artCollectionContent.SetActive(false);
        }
    }
}
